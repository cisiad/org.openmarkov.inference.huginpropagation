/*
 * Copyright 2011 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.inference.huginPropagation;

import java.util.List;

import org.openmarkov.core.exception.CanNotDoEditException;
import org.openmarkov.core.exception.ConstraintViolationException;
import org.openmarkov.core.exception.DoEditException;
import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NonProjectablePotentialException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.exception.WrongCriterionException;
import org.openmarkov.core.inference.annotation.InferenceAnnotation;
import org.openmarkov.core.inference.heuristic.EliminationHeuristic;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.potential.Intervention;
import org.openmarkov.core.model.network.type.BayesianNetworkType;

/**
 * Create objects for Hugin algorithm inference. This class is nearly empty
 * because the algorithm is implemented in <code>HuginForest</code> construction
 * and in <code>openmarkov.inference.ClusterPropagation</code>.
 * @author marias
 * @author fjdiez
 * @version 1.0
 * @since OpenMarkov 1.0
 * @see openmarkov.graphs.Graph
 * @see openmarkov.inference.heuristics.CanoMoralElimination
 * @see openmarkov.networks.EvidenceCase
 * @see openmarkov.networks.Finding
 */
@InferenceAnnotation(name = "HuginPropagation")
public class HuginPropagation extends ClusterPropagation
{
    // Constructor
    /**
     * @param probNet <code>ProbNet</code>
     * @throws ConstraintViolationException
     * @throws NotEvaluableNetworkException
     */
    public HuginPropagation (ProbNet probNet)
        throws NotEvaluableNetworkException
    {
        super (probNet);
    }

    // Methods
    @Override
    /** Creates a HuginForest for a Markov network having a root clique 
     *   containing all the<code>queryNodes</code>. 
     * @param markovNet <code>ProbNet</code> This Markov network will be 
     *   destroyed.
     * @param heuristic <code>EliminationHeuristic</code>
     * @param queryNodes <code>ArrayList</code> of <code>Node</code>
     * @param pNESupport <code>PNESupport</code> 
     * @return A <code>HuginForest</code> */
    protected ClusterForest createForest (ProbNet markovNet,
                                          EliminationHeuristic heuristic,
                                          List<Node> queryNodes)
        throws ConstraintViolationException,
        CanNotDoEditException,
        NonProjectablePotentialException,
        WrongCriterionException,
        DoEditException
    {
        return new HuginForest (markovNet, heuristic, queryNodes);
    }

    @Override
    /** Creates a HuginForest from a MarkovNet having a rootClique containing
     * all the<code>queryNodes</code>. 
     * @return A <code>HuginForest</code> 
     * @param markovNet <code>ProbNet</code> This Markov network will be 
     *   destroyed.
     * @param heuristic <code>EliminationHeuristic</code> */
    protected ClusterForest createForest (ProbNet markovNet,
                                          EliminationHeuristic heuristic)
        throws DoEditException,
        NonProjectablePotentialException,
        WrongCriterionException
    {
        return new HuginForest (markovNet, heuristic);
    }

    /**
     * Creates a HuginForest for a MarkovNet.
     * @return A <code>HuginForest</code>
     * @param markovNet <code>ProbNet</code> This Markov network will be
     *            destroyed.
     * @throws WrongCriterionException
     * @throws NonProjectablePotentialException
     * @throws DoEditException
     * @throws NotEnoughMemoryException
     */
    protected ClusterForest createForest (ProbNet markovNet)
        throws DoEditException,
        NonProjectablePotentialException,
        WrongCriterionException
    {
        return new HuginForest (markovNet, heuristic);
    }
    
	public static void checkEvaluability(ProbNet probNet) throws NotEvaluableNetworkException {
		if(!probNet.getNetworkType().equals(BayesianNetworkType.getUniqueInstance()))
			throw new NotEvaluableNetworkException("Hugin propagation can currently only evaluate Bayesian networks.");
	}

	@Override
	public Intervention getOptimalStrategy() throws IncompatibleEvidenceException,
			UnexpectedInferenceException {
		// TODO Auto-generated method stub
		return null;
	}
}
