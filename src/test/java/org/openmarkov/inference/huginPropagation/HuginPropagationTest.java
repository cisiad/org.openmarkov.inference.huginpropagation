/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.inference.huginPropagation;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.inference.InferenceAlgorithm;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.Finding;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.inference.huginPropagation.ClusterPropagation;
import org.openmarkov.inference.huginPropagation.ClusterPropagation.StorageLevel;
import org.openmarkov.inference.huginPropagation.HuginPropagation;

/**
 * @author Manuel Arias
 *
 */
public class HuginPropagationTest {

	private final double maxError = 0.0001;

	@Before public void setUp() throws Exception {
	}

	public InferenceAlgorithm buildInferenceAlgorithm(ProbNet probNet) throws NotEvaluableNetworkException {
		HuginPropagation algorithm = null;
		algorithm = new HuginPropagation(probNet);
		return algorithm;
	}

	@Test
	public void testHuginNoEvidence() throws Exception {
		ProbNet probNet;
		HashMap<Variable, TablePotential> posteriorProbabilities;

		for (StorageLevel storage : StorageLevel.values()) {
			// first test: asia.elv
			// gets a priori probabilities
			probNet = BNAsia.buildBN_asia_java(); 

			List<List<Variable>> variables = new ArrayList<List<Variable>>();
			variables.add(probNet.getChanceAndDecisionVariables());

			posteriorProbabilities = null;
			ClusterPropagation propagation = new HuginPropagation(probNet);
			((HuginPropagation)propagation).setStorageLevel(storage);
			propagation.compilePriorPotentials();

			propagation.setPreResolutionEvidence(new EvidenceCase());

			posteriorProbabilities = propagation.getProbsAndUtilities();
			// test probabilities
			Variable A = probNet.getVariable("VisitToAsia");
			TablePotential tpA = posteriorProbabilities.get(A);
			assertEquals(tpA.values[0], 0.9900, maxError);
			assertEquals(tpA.values[1], 0.0100, maxError);

			Variable S = probNet.getVariable("Smoker");
			TablePotential tpS = posteriorProbabilities.get(S);
			assertEquals(tpS.values[0], 0.5, maxError);
			assertEquals(tpS.values[1], 0.5, maxError);

			Variable T = probNet.getVariable("Tuberculosis");
			TablePotential tpT = posteriorProbabilities.get(T);
			assertEquals(tpT.values[0], 0.9896, maxError);
			assertEquals(tpT.values[1], 0.0104, maxError);

			Variable L = probNet.getVariable("LungCancer");
			TablePotential tpL = posteriorProbabilities.get(L);
			assertEquals(tpL.values[0], 0.945, maxError);
			assertEquals(tpL.values[1], 0.055, maxError);

			Variable B = probNet.getVariable("Bronchitis");
			TablePotential tpB = posteriorProbabilities.get(B);
			assertEquals(tpB.values[0], 0.55, maxError);
			assertEquals(tpB.values[1], 0.45, maxError);

			Variable E = probNet.getVariable("TuberculosisOrCancer");
			TablePotential tpE = posteriorProbabilities.get(E);
			assertEquals(tpE.values[0], 0.935172, maxError);
			assertEquals(tpE.values[1], 0.064828, maxError);

			Variable X = probNet.getVariable("X-ray");
			TablePotential tpX = posteriorProbabilities.get(X);
			assertEquals(tpX.values[0], 0.8897096, maxError);
			assertEquals(tpX.values[1], 0.1102904, maxError);

			Variable D = probNet.getVariable("Dyspnea");
			TablePotential tpD = posteriorProbabilities.get(D);
			assertEquals(tpD.values[0], 0.5640294, maxError);
			assertEquals(tpD.values[1], 0.4359706, maxError);
		}
	}

	@Test
	public void testHuginWithEvidence() throws Exception {
		ProbNet probNet;
		HashMap<Variable, TablePotential> posteriorProbabilities;

		for (StorageLevel storage : StorageLevel.values()) {
			// first test: asia.elv
			// gets a priori probabilities
			probNet = BNAsia.buildBN_asia_java();
			Variable X = probNet.getVariable("X-ray");
			EvidenceCase evidence = new EvidenceCase();
			evidence.addFinding(new Finding(X, X.getStateIndex("yes")));

			List<List<Variable>> variables = new ArrayList<List<Variable>>();
			variables.add(probNet.getChanceAndDecisionVariables());

			posteriorProbabilities = null;
			ClusterPropagation propagation = new HuginPropagation(probNet);
			((HuginPropagation)propagation).setStorageLevel(storage);
			propagation.compilePriorPotentials();
			propagation.setPreResolutionEvidence(evidence);

			propagation.setPreResolutionEvidence(new EvidenceCase());

			posteriorProbabilities = propagation.getProbsAndUtilities();
			// test probabilities
			Variable A = probNet.getVariable("VisitToAsia");
			TablePotential tpA = posteriorProbabilities.get(A);
			assertEquals(tpA.values[0], 0.9868, maxError);
			assertEquals(tpA.values[1], 0.0132, maxError);

			Variable S = probNet.getVariable("Smoker");
			TablePotential tpS = posteriorProbabilities.get(S);
			assertEquals(tpS.values[0], 0.3122, maxError);
			assertEquals(tpS.values[1], 0.6878, maxError);

			Variable T = probNet.getVariable("Tuberculosis");
			TablePotential tpT = posteriorProbabilities.get(T);
			assertEquals(tpT.values[0], 0.9076, maxError);
			assertEquals(tpT.values[1], 0.0924, maxError);

			Variable L = probNet.getVariable("LungCancer");
			TablePotential tpL = posteriorProbabilities.get(L);
			assertEquals(tpL.values[0], 0.5113, maxError);
			assertEquals(tpL.values[1], 0.4887, maxError);

			Variable B = probNet.getVariable("Bronchitis");
			TablePotential tpB = posteriorProbabilities.get(B);
			assertEquals(tpB.values[0], 0.4937, maxError);
			assertEquals(tpB.values[1], 0.5063, maxError);

			Variable E = probNet.getVariable("TuberculosisOrCancer");
			TablePotential tpE = posteriorProbabilities.get(E);
			assertEquals(tpE.values[0], 0.4240, maxError);
			assertEquals(tpE.values[1], 0.5760, maxError);

			Variable D = probNet.getVariable("Dyspnea");
			TablePotential tpD = posteriorProbabilities.get(D);
			assertEquals(tpD.values[0], 0.3592, maxError);
			assertEquals(tpD.values[1], 0.6408, maxError);
		}
	}

}
